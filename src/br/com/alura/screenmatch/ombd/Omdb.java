package br.com.alura.screenmatch.ombd;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Omdb {
    private String endereco;
    private String nomeFilme;

    public Omdb(String nomeFilme) {
        this.nomeFilme = nomeFilme.replace(" ",
                "+");
        this.endereco = "http://www.omdbapi.com/?t=" + this.nomeFilme
                + "&apikey=a594fbef";
    }

    public String pegarRequisicao() {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(endereco))
                    .build();
            HttpResponse<String> response = client
                    .send(request, HttpResponse.BodyHandlers.ofString());

            return response.body();
        } catch (IllegalArgumentException | IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getNomeFilme() {
        return nomeFilme;
    }

    public void setNomeFilme(String nomeFilme) {
        this.nomeFilme = nomeFilme;
    }
}
